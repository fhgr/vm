#!/bin/bash

# This is a update script for the FHGR Photonics + Mobile Robotics VM. Currently
# it uses Kubuntu 20.04(.1) (LTS) and VirtualBox 6.1.2.

# Here is the log how the VM image was updated. All options and settings are default
# except for what was noted down below.


# Import FHGR Photonics + Mobile Robotics VM

# VirtualBox Manager:
#     1. Appliance importieren ...
#     2. Quelle: [...]\FHGR Photonics + MobileRobotics VM 20.04.ovf
#        Einstellungen:
#          Name: FHGR Photonics + MobileRobotics VM 20.04 update#
#          Version: [version]
#          Beschreibung: [...]
#          Gast-Betriebssystem: Ubuntu (64-bit)
#          CPU: 2
#          RAM: 4096 MB
#          DVD-Laufwerk: [X]
#          USB-Controller: [X]
#          Soundkarte: [X] ICH AC97
#          Netzwerkadapter: [X] Intel PRO/1000 MT Desktop (82540EM)
#          IDE-Controller: PIIX4
#          IDE-Controller: PIIX4
#          SATA-Controller: AHCI
#            Plattenabbild: FHGR Photonics + MobileRobotics VM 20.04-disk001.vmdk
#          Basis-Ordner: [...]
#          Primäre Gruppe: /
#
#          Basisordner der virtuellen Maschine: [...]
#          MAC-Adressen-Richtlinie: Neue MAC-Adressen für alle Netzwerkadapter generieren
#          Zusätzliche Optionen: [x] Festplatten als VDI importieren
#     3. Importieren

# Update Kubunt 20.04 (LTS) OS;
#   0. (Für anfänglichen Testlauf: Snapshot VM; choose 'Erzeugen' from the menu)
#   1. VM starten
#   2. Konsole für Anpassungen, z.B.: $ sudo apt -y purge unattended-upgrades
#   3. Konsole für Update: ~$ wget -O ~/FHGR-Photonics+MobileRobotics-Kubuntu-20.04-update.sh https://gitlab.com/fhgr/vm/-/raw/master/FHGR-Photonics+MobileRobotics-Kubuntu-20.04-update.sh
#                          ~$ chmod +x FHGR-Photonics+MobileRobotics-Kubuntu-20.04-update.sh
#                          ~$ source ./FHGR-Photonics+MobileRobotics-Kubuntu-20.04-update.sh
#      (optimaler Weise lassen Sie Ihren Computer alleine bis das Setup beendet ist - der Grund ist dass die Zwischenablage der
#      VM geleert wird und wenn Sie weiter arbeiten und etwas in die Zwischenablage kopieren wird das in der VM aufgezeichnet)
#   4. Host (win) Konsole: $ cd "C:\Users\solerursin\LocalDrive\VirtualBox VMs\FHGR Photonics _ MobileRobotics VM 20.04 update#"
#                          $ "c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" modifymedium --compact "FHGR Photonics + MobileRobotics VM 20.04-disk001.vdi"

# Vorbereiten der Auslieferung;
#   1. Test durch Entwickler
#   2. Ausliefern (# = Nummer des Updates 1, 2, 3, ... / [version] bleibt wie zuvor z.B. 1.2) - based on Kubuntu focal 20.04 (LTS)
#     1. Appliance exportieren ...
#     2. Experten-Modus
#        Name: FHGR Photonics + MobileRobotics VM 20.04
#        Produkt:
#        Produkt-URL:
#        Hersteller: FHGR
#        Hersteller-URL:
#        Version: [version].#
#        Beschreibung: This VM provides a pre-installed and setup software environment for studies and practical work for FHGR Photonics + MobileRobotics. It is based on Kubuntu 20.04 (LTS) and VirtualBox 6.1.2.
#        Lizenz:
#
#        Format: Open Virtualization Format 1.0
#        Datei: C:\Users\solerursin\switchdrive\FHGR Photonics + MobileRobotics VM 20.04 v[version].#\VM Appliance\FHGR Photonics + MobileRobotics VM 20.04.ovf
#               (erlaubt nach Wunsch Nutzung der Appliance oder nur des Images)
#        MAC-Adressen-Richtlinie: Alle Netzwerkadapter-MAC-Adressen entfernen
#        Zusätzlich: [x] Schreibe Manifest-Datei
#                    [ ] ISO-Abbilder mit einbeziehen
#     3. Exportieren
#   3. Beta-Test durch Nutzer (Dozenten, wenn möglich Studenten)
#   4. Anpassen wo nötig basierend auf Rückmeldungen, wiederholen angefangen bei Punkt 1.

# User setup;
#   1. Download VirtualBox 6.1.2 and Extension Pack:
#        https://www.virtualbox.org/wiki/Download_Old_Builds_6_1
#   2. Install VirtualBox 6.1.2 and Extension Pack:
#        https://www.virtualbox.org/manual/ch01.html#intro-installing
#   3. Download the appliance containing a virtual disk and config files that describe the setup
#   4. Use the exported appliance or the virtual disc only, use config similar to the one described before
#   5. Create a snapshot of the VM; choose 'Erzeugen' from the menu
#      Do this as VERY FIRST step BEFORE doing anything else, NEVER EVER delete this first snapshot!
#      (this is your fall-back for the worst case everything crashes or gets messy - otherwise you
#      you have to start from step 1. again)
#   6. You can always add more snapshots at later points if you want or need it (even in a running system)
#   7. Geräte > Gemeinsame Ordner > Gemeinsame Ordner...
#      Hinzufügen: Ordner-Pfad; (Windows Dokumente Verzeichnis auswählen)
#                  Ordner-Name: Documents
#                  [ ] Nur lesbar
#                  [x] Automatisch einbinden
#                  [x] Permanent erzeugen
#      (Das Verzeichnis wird nach /media/sf_Documents gemountet und ist dort verfügbar, das wird auch
#      für andere Verzeichnisse wie Switch-Drive etc. empfohlen)
#   8. If it works, in order to save this setting create another snapshot and delete the first one. This
#      causes the first snapshot to be merged into the disk image and making it permanent.


## General

# Stop this script on any error occuring
#set -e
set -o errexit -o pipefail -o nounset

# Reset clipboard, console and Start Menu Search histories (Kate and Start Menu Recent done manually, see above 7.+8.)
qdbus org.kde.klipper /klipper org.kde.klipper.klipper.clearClipboardHistory
history -c                      # needs this script to be run using 'source'
history -w                      #
rm -rf ~/.local/share/RecentDocuments/  # Start Menu Search etc.
# (Dolphin bookmarks: ~/.local/share/user-places.xbel)


## Update and Upgrades

# Update repos
sudo apt -y update             # update der repo Listen, bei Installation der Gasterweiterungen oben schon gemacht
sudo apt -y upgrade            # update der installierten Software (Zustand danach undefiniert, hängt vom Datum ab...)
sudo apt autoremove            # ACHTUNG: Prüfen welche Pakete entfernt werden und ob das Probleme verursachen könnte

# Update pip packages
# https://stackoverflow.com/questions/2720014/how-to-upgrade-all-python-packages-with-pip
#pip3 list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip3 install -U
# SKIPPED: update breaks compatibility and reqirements for tensorflow

# Update eclipse
# (skipped)

# Update Qucs
# (skipped)


## Prepare VM disk for deployment

# Clear apt cache
sudo du -sh /var/cache/apt/archives/
sudo apt clean

# Remove this setup script
rm ~/FHGR-Photonics+MobileRobotics-Kubuntu-20.04-update.sh

# Shrink drive before exporting: https://superuser.com/questions/529149/how-to-compact-virtualboxs-vdi-file-size

echo
echo 'Nullify free space in order to shrink virtual disk afterwards.'
echo 'VM should be run w/o snapshots active, e.g. all should be merged before.'

#read -p "Press any key to continue..."

# https://github.com/chef/bento/blob/master/packer_templates/_common/minimize.sh

# Whiteout root
count=$(df --sync -kP / | tail -n1  | awk -F ' ' '{print $4}')
count=$(($count-1))
sudo dd if=/dev/zero of=/tmp/whitespace bs=1M count=$count || echo "dd exit code $? is suppressed";
sudo rm /tmp/whitespace

# Whiteout /boot
count=$(df --sync -kP /boot | tail -n1 | awk -F ' ' '{print $4}')
count=$(($count-1))
sudo dd if=/dev/zero of=/boot/whitespace bs=1M count=$count || echo "dd exit code $? is suppressed";
sudo rm /boot/whitespace

#read -p "Press any key to continue..."

shutdown -h now
