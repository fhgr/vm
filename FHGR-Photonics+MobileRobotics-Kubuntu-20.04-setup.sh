#!/bin/bash

# This is a setup script for the FHGR Photonics + Mobile Robotics VM. Currently
# it uses Kubuntu 20.04(.1) (LTS) and VirtualBox 6.1.2.

# Here is the log how the VM image was setup. All options and settings are default
# except for what was noted down below.

# Possible ways to automate this setup further include fabric or packer as well
# as using server or minimal mediums or a pre-setup VM image from osboxes.org.

# Permission to use wallpaper granted by Julius Saputra:
# https://www.juliussaputra.com/travel-photography


# Install VirtualBox 6.1.2

# Setup VirtualBox VM; choose 'Neu' from the menu
#   Name: FHGR Photonics + MobileRobotics VM 20.04
#   Typ: Linux
#   Version: Ubuntu (64-bit)
#   Speichergrösse: 4096 MB [1/4 of my machine]
#   Festplatte erzeugen
# Virtuelle Festplatte
#   Dateigrösse: 25 GB
#   Dateityp der Festplatte: VDI
#   Art der Speicherung: dynamisch alloziert
# Adopt VirtualBox VM setup; choose 'Ändern' from the menu
#   (Maschine > Ändern... >) Allgemein > Erweitert > Gemeinsame Zwischenablage: bidirektional
#   (Maschine > Ändern... >) Allgemein > Erweitert > Drag'n'Drop: bidirektional
#   System > Prozessor > Prozessoren: 2 [1/4 of my machine]
#   Anzeige > Bildschirm > Grafikspeicher: 64 MB
#   Massenspeicher > CD: kubuntu-20.04-desktop-amd64.iso

# Setup Kubunt 20.04 (LTS) OS;
#   1. VM starten
#   (VirtualBox; Medium für Start auswählen > Abbrechen)
#   (Installer Fenster hat Probleme und ist nicht vollständig zu sehen; Doppelklick auf die Titelleiste um zu Maximieren hilft)
#   2. Deutsch
#   3. Kubuntu installieren
#   4. Tastaturbelegung: Switzerland
#      Variante: Switzerland - German (Switzerland, legacy)
#   5. Minimale Installation
#      [x] Während Kubuntu installiert wird Aktualisierungen herunterladen
#      [ ] Installieren Sie Software von Drittanbietern [...]
#   6. Geführt - vollständige Festplatte verwenden
#   7. Region: Schweiz
#      Zeitzone: Schweiz Zeit
#   8. Ihr Name: FHGR
#      (Benutzernamen: fhgr)
#      Passwort: fhgr
#      (Name des Rechners: fhgr-VirtualBox)
#      (x) Automatisch anmelden
#   9. Jetzt neu starten
#  10. Herunterfahren

# [Checkpoint 1: VM disk size at this point is ~6.47 GB (not shrinked) - osboxes.org using 7-zip is ~1.5 GB]
# => VirtualBox beenden und Backup der Festplatte (.vdi) erstellen

# Snapshot VM; choose 'Erzeugen' from the menu

# Konfiguration der VM;
#   1. VM starten
#      (warten bis KDE gestartet ist)
#      (falls Aktualisierungen gemeldet werden, diese ignorieren sie werden später vom Benutzer oder unattended angewendet, siehe unten)
#   2. Geräte > Gasterweiterungen einlegen...
#      Pop-up: Mit Dateiverwaltung öffnen (um Gasterweiterungen CD einzulegen/mounten)
#   3. Konsole: $ sudo apt update; sudo apt -y install build-essential dkms
#               $ cd /media/fhgr/VBox_GAs_6.1.2/
#               $ sudo ./VBoxLinuxAdditions.run; shutdown -h now
#               (herunterfahren könnte hier einige Zeit dauern - nur Geduld)
#   4. VM konfigurieren:
#      * Maschine > Ändern... > Massenspeicher > VBoxGuestAdditions.iso > Entfernt das virtuelle Medium aus dem Laufwerk
#   5. Snapshot VM löschen (merge mit Festplattenimage)
#      (je nach Wahl diesen Schritt für einen "dry-run" oder Test überspringen - wenn ok den ganzen Block wiederholen)
#   6. VM starten (von nun an ist copy'n'paste und drag'n'drop zwischen Host und Gast möglich)
#   7. KDE konfigurieren:
#      * Systemeinstellungen > Verhalten des Arbeitsbereichs > Bildschirmsperre
#           Bildschirm sperren: [ ] Bildschirm automatisch nach: ...
#                               [ ] Nach Standby
#           Anwenden
#      * Systemeinstellungen > Anzeige und Monitor > Compositor
#           [ ] Compositor beim Start aktivieren
#           Anwenden
#      * Systemeinstellungen > Energieverwaltung > Energiesparmodus
#           Am Netzkabel: [ ] Bildschirm-Energieverwaltung
#           Im Akkubetrieb: [ ] Bildschirm-Energieverwaltung
#                           [ ] Sitzung in den Standby-Modus versetzen
#           Im Akkubetrieb bei niedrigem Ladestand: [ ] Bildschirm-Energieverwaltung
#                                                   [ ] Sitzung in den Standby-Modus versetzen
#           Anwenden
#      * Dolphin: per drag'n'drop "/media" als Verknüpfung hinzufügen in Orte zuunterst, also unter "Papierkorb"
#      * Start > Verlauf > rechts-klick auf ein Element; "Alles vergessen" (auch über Systemsteuerung möglich)
#   8. Konsole: ~$ wget -O ~/FHGR-Photonics+MobileRobotics-Kubuntu-20.04-setup.sh https://gitlab.com/fhgr/vm/-/raw/master/FHGR-Photonics+MobileRobotics-Kubuntu-20.04-setup.sh
#               ~$ chmod +x FHGR-Photonics+MobileRobotics-Kubuntu-20.04-setup.sh
#               ~$ source ./FHGR-Photonics+MobileRobotics-Kubuntu-20.04-setup.sh
#      (optimaler Weise lassen Sie Ihren Computer alleine bis das Setup beendet ist - der Grund ist dass die Zwischenablage der
#      VM geleert wird und wenn Sie weiter arbeiten und etwas in die Zwischenablage kopieren wird das in der VM aufgezeichnet)
#   9. Host (win) Konsole: $ cd "c:\Users\solerursin\VirtualBox VMs\FHGR Photonics _ MobileRobotics VM 20.04"
#                          $ "c:\Program Files\Oracle\VirtualBox\VBoxManage.exe" modifymedium --compact "FHGR Photonics _ MobileRobotics VM 20.04.vdi"

# [Checkpoint 2: VM disk size is ~14.97 GB (not shrinked) respective ~11.2 GB (shrinked)]
# => Falls snapshots gelöscht; VirtualBox beenden und Backup der Festplatte (.vdi) erstellen

# Snapshot VM; choose 'Erzeugen' from the menu

# Vorbereiten der Auslieferung;
#   1. Test durch Entwickler
#   2. Sicherungspunkte/Snapshots löschen (und damit mergen)
#   3. Ausliefern (9.4. "Easter Gift" focal beta, 23.4. v1.0 focal, 13.5. v1.1) - based on Kubuntu focal 20.04 (LTS)
#     1. Appliance exportieren
#     2. Experten-Modus
#        Name: FHGR Photonics + MobileRobotics VM 20.04
#        Produkt:
#        Produkt-URL:
#        Hersteller: FHGR
#        Hersteller-URL:
#        Version: [version]
#        Beschreibung: This VM provides a pre-installed and setup software environment for studies and practical work for FHGR Photonics + MobileRobotics. It is based on Kubuntu 20.04 (LTS) and VirtualBox 6.1.2.
#        Lizenz:
#
#        Format: Open Virtualization Format 1.0
#        Datei: D:\solerursin\switchdrive\FHGR Photonics + MobileRobotics VM 20.04 v[version]\VM Appliance\FHGR Photonics + MobileRobotics VM 20.04.ovf
#               (erlaubt nach Wunsch Nutzung der Appliance oder nur des Images)
#        MAC-Adressen-Richtlinie: Alle Netzwerkadapter-MAC-Adressen entfernen
#        Zusätzlich: [x] Schreibe Manifest-Datei
#                    [ ] ISO-Abbilder mit einbeziehen
#     3. Exportieren
#   4. Beta-Test durch Nutzer (Dozenten, wenn möglich Studenten)
#   5. Anpassen wo nötig basierend auf Rückmeldungen, wiederholen angefangen bei Punkt 1.

# User setup;
#   1. Download VirtualBox 6.1.2 and Extension Pack:
#        https://www.virtualbox.org/wiki/Download_Old_Builds_6_1
#   2. Install VirtualBox 6.1.2 and Extension Pack:
#        https://www.virtualbox.org/manual/ch01.html#intro-installing
#   3. Download the appliance containing a virtual disk and config files that describe the setup
#   4. Use the exported appliance or the virtual disc only, use config similar to the one described before
#   5. Create a snapshot of the VM; choose 'Erzeugen' from the menu
#      Do this as VERY FIRST step BEFORE doing anything else, NEVER EVER delete this first snapshot!
#      (this is your fall-back for the worst case everything crashes or gets messy - otherwise you
#      you have to start from step 1. again)
#   6. You can always add more snapshots at later points if you want or need it (even in a running system)
#   7. Geräte > Gemeinsame Ordner > Gemeinsame Ordner...
#      Hinzufügen: Ordner-Pfad; (Windows Dokumente Verzeichnis auswählen)
#                  Ordner-Name: Documents
#                  [ ] Nur lesbar
#                  [x] Automatisch einbinden
#                  [x] Permanent erzeugen
#      (Das Verzeichnis wird nach /media/sf_Documents gemountet und ist dort verfügbar)
#   8. If it works, in order to save this setting create another snapshot and delete the first one. This
#      causes the first snapshot to be merged into the disk image and making it permanent.


## General

# (KDE konfigurieren, siehe oben; Energieverwaltung, Bildschirmsperre, Compositor)

# Stop this script on any error occuring
#set -e
set -o errexit -o pipefail -o nounset

# Setup to allow security updates only and do them
# https://phoenixnap.com/kb/automatic-security-updates-ubuntu
# https://help.ubuntu.com/lts/serverguide/automatic-updates.html
systemctl status unattended-upgrades
#diff -u /etc/apt/apt.conf.d/50unattended-upgrades /etc/apt/apt.conf.d/50unattended-upgrades.patched > ~/50unattended-upgrades.patch
sudo wget -O - https://gitlab.com/fhgr/vm/-/raw/master/50unattended-upgrades.patch | sudo patch /etc/apt/apt.conf.d/50unattended-upgrades     # apply patch (on)
#sudo wget -O - https://gitlab.com/fhgr/vm/-/raw/master/50unattended-upgrades.patch | sudo patch -R /etc/apt/apt.conf.d/50unattended-upgrades  # undo patch (off)
head -n 20 /etc/apt/apt.conf.d/50unattended-upgrades
sudo sh -c 'echo APT::Periodic::Download-Upgradeable-Packages \"1\"\; >> /etc/apt/apt.conf.d/20auto-upgrades'
sudo sh -c 'echo APT::Periodic::AutocleanInterval \"7\"\; >> /etc/apt/apt.conf.d/20auto-upgrades'
cat /etc/apt/apt.conf.d/20auto-upgrades
#sudo unattended-upgrades --dry-run --debug
sudo unattended-upgrades

# Setup groups for default user
sudo usermod -aG vboxsf $USER   # add to group vboxsf in order to allow access to shared drive
sudo usermod -aG dialout $USER  # add to dialout group in order to allow serial device and USB access

# Set wallpaper (desktop background) - files in repo need to be public
sudo wget -O /usr/share/wallpapers/FHGR.jpg https://gitlab.com/fhgr/vm/-/raw/master/RheinSchlucht_whilenotworking.jpg
qdbus org.kde.plasmashell /PlasmaShell org.kde.PlasmaShell.evaluateScript 'var allDesktops = desktops();print (allDesktops);for (i=0;i<allDesktops.length;i++) {d = allDesktops[i];d.wallpaperPlugin = "org.kde.image";d.currentConfigGroup = Array("Wallpaper", "org.kde.image", "General");d.writeConfig("Image", "file:///usr/share/wallpapers/FHGR.jpg")}'
#sudo convert -background '#0008' -fill white -gravity west -size 400x60 caption:'FHGR Photonics + MobileRobotics VM 20.04\nbasierend auf Kubuntu 20.04 LTS\nDatum: 16.04.2020 (v0.2)' /usr/share/wallpapers/FHGR.jpg +swap -geometry +18+180 -gravity northwest -composite /usr/share/wallpapers/FHGR-watermark.jpg
# (may be a wallpaper with FHGR logo would be even better - wallpaper can also be put on gitlab in order to have a http download url)
# (open alternatives from wiki commons, e.g.: https://commons.wikimedia.org/wiki/File:Kropfenstein_DJIc.JPG)

# Reset clipboard, console and Start Menu Search histories (Kate and Start Menu Recent done manually, see above 7.+8.)
qdbus org.kde.klipper /klipper org.kde.klipper.klipper.clearClipboardHistory
history -c                      # needs this script to be run using 'source'
history -w                      #
rm -rf ~/.local/share/RecentDocuments/  # Start Menu Search etc.
# (Dolphin bookmarks: ~/.local/share/user-places.xbel)

read -p "Press any key to continue..."

# Update repos
#sudo apt update             # update der repo Listen, bei Installation der Gasterweiterungen oben schon gemacht
#sudo apt upgrade            # update der installierten Software (Zustand danach undefiniert, hängt vom Datum ab...)

# Install basic packages
sudo apt -y install git                   # +40 MB
sudo apt -y install meld                  # +90 MB
#sudo apt -y install kdiff3                # +4 MB
sudo apt -y install pdfarranger           # +2 MB, (pdf printer installed already)
# ? noscript for firefox
sudo apt -y install kate                  # +0 B, als manuell installiert festgelegt (Kubuntu uses KDE/Qt)
sudo apt -y install okular                # +0 B, als manuell installiert festgelegt (Kubuntu uses KDE/Qt)
sudo apt -y install mc                    # +8 MB
sudo apt -y install net-tools             # +870 KB, (ifconfig, etc.)
# csv viewer; use shared drive and software on host, e.g. libreoffice calc, excel, etc.
# printer; use print to file and pdfarranger to produce PDF output


## Install python3

# General
sudo apt -y install python3-pip           # +28 MB
sudo apt -y install python3-venv          # +40 KB

# Udo - Signalverarbeitung
sudo apt -y install python3-matplotlib    # >= 3.1, +63 MB
sudo apt -y install python3-pyaudio       # >= 2.11, +340 KB
sudo apt -y install python3-pyqt5         # +0 B, als manuell installiert festgelegt (Kubuntu uses KDE/Qt)

# Udo - Bildverarbeitung
sudo apt -y install python3-matplotlib    # >= 3.1, +63 MB
sudo apt -y install python3-numpy         # +0 B, als manuell installiert festgelegt (from python3-matplotlib)
#pip3 install opencv-contrib-python        # +35 MB
sudo apt -y install python3-opencv        # opencv-contrib-dev, +387 MB
sudo apt -y install ffmpeg                # +6 MB
sudo apt -y install python3-scipy         # +48 MB
sudo apt -y install python3-sklearn       # python3-scikit-learn, +44 MB
sudo apt -y install python3-skimage       # python3-scikit-image, +111 MB
#sudo apt install cmake                    # +29 MB
#pip3 install dlib                         # +? MB, (compiles, needs cmake)

# Udo - Bildverarbeitung 3 (deeplearning)
sudo apt -y install python3-sklearn       # python3-scikit-learn, +44 MB
#pip3 install jupyter                      # +? MB
sudo apt -y install jupyter-notebook      # +217 MB
sudo apt -y install jupyter-qtconsole     # +19 KB
pip3 install tensorflow                   # +? MB (> 517 MB), (installs scipy again)
sudo apt -y install python3-keras         # +76 MB
sudo apt -y install python3-seaborn       # +2 MB
sudo apt -y install python3-pandas        # +0 B, als manuell installiert festgelegt (from python3-seaborn)
sudo apt -y install python3-pillow        # +0 B, als manuell installiert festgelegt

# tensorflow nicht in repos; https://packages.ubuntu.com/search?suite=default&section=all&arch=any&keywords=tensorflow&searchon=all

# Ursin - Optische Messtechnik 1
sudo apt -y install python3-matplotlib    # >= 3.1, +63 MB
sudo apt -y install python3-numpy         # +0 B, als manuell installiert festgelegt (from python3-matplotlib)
pip3 install mplcursors                   # +20 KB, (nicht in repos)
pip3 install pycwt                        # +1 MB, (wavelet spectra)
#sudo apt install mayavi2                  # +247 MB, (3D plotting like surfaces etc.)
#pip3 install plotly                       # +? MB, (buggy version in repo also; python3-plotly)
pip3 install pylustrator                  # +~200 KB
#sudo apt install python3-numba            # +207 MB, (compile and speed-up calculation, e.g. numpy)
#pip3 install spyder-notebook              # +>100 MB, (is big and installs spyder4 via pip)
sudo apt -y install jupyter-nbconvert     # +~30 KB, (cli export with more options)

# Ursin - Labor, Elektronik, etc.
sudo apt -y install python3-serial        # +480 KB
sudo apt -y install numpy-stl             # +240 KB, (handle and plot CAD/STL data)
#sudo apt -y install inkscape              # +~25MB, (handle SVG->PDF in jupyterlab)

# Mathematik/Physik
sudo apt -y install spyder3               # +100 MB, (installs python2 stuff also)

# Philipp - Informatik
sudo apt -y install python3-pyqt5         # +0 B, als manuell installiert festgelegt (Kubuntu uses KDE/Qt)
sudo apt -y install python3-seaborn       # +2 MB
sudo apt -y install python3-pytest        # +0 B, als manuell installiert festgelegt
sudo apt -y install python3-flask         # +3 MB

# Andi - ???
sudo apt -y install ipython3              # +40 KB
pip3 install jupyterlab                   # +8 MB, (nicht in repos)
sudo apt -y install dvipng                # +80 KB, (benötigt von jupyterlab für matplotlib Ausgabe)

# Ulrich - Elektronik ? und Elektrotechnik ?
sudo apt -y install python3-sympy         # +31 MB


## Install C/C++ toolchain

# Philipp - Informatik, Bildverarbeitung, Produktentwicklung, IRCADI
sudo apt -y install git                   # +40 MB
sudo apt -y install gitk                  # +2 MB
sudo apt -y install meld                  # +90 MB
sudo apt -y install terminator            # +4 MB
sudo apt -y install mc                    # +8 MB
sudo apt -y install g++                   # +0 B, als manuell installiert festgelegt
sudo apt -y install clang                 # +478 MB
sudo apt -y install clang-tidy            # +41 MB
sudo apt -y install clang-tools           # +26 MB
sudo apt -y install doxygen               # +44 MB
sudo apt -y install valgrind              # +90 MB
sudo apt -y install qtbase5-dev           # +41 MB
sudo apt -y install qtcreator             # +464 MB, (enthält Qt Designer)
sudo apt -y install cppreference-doc-en-qch  # +47 MB
sudo apt -y install libopencv-dev         # +153 MB
sudo apt -y install libopencv-contrib-dev  # +136 MB
sudo apt -y install libncurses-dev        # +0 B, als manuell installiert festgelegt
sudo apt -y install libgtest-dev          # +22 MB
sudo apt -y install libgmock-dev          # +23 MB

# Philipp - 3D-Bildverarbeitung
#sudo apt -y install cmake libpcl-dev libpcl-doc pcl-tools python3-pcl  # +~1.4 GB

# Ursin
#sudo apt install upx                      # +3 MB, (compress executables)

# Gion-Pol (https://linuxconfig.org/eclipse-ide-for-c-c-developers-installation-on-ubuntu-20-04)
sudo apt -y install default-jre              # +178 MB
wget http://ftp.fau.de/eclipse/technology/epp/downloads/release/2020-06/R/eclipse-cpp-2020-06-R-linux-gtk-x86_64.tar.gz
sudo tar xf eclipse-cpp-2020-06-R-linux-gtk-x86_64.tar.gz -C /opt  # +~280 MB
rm eclipse-cpp-2020-06-R-linux-gtk-x86_64.tar.gz
#sudo ln -s /opt/eclipse/eclipse /usr/local/bin/
sudo sh -c 'echo "[Desktop Entry]" >> /usr/share/applications/eclipse.desktop'
sudo sh -c 'echo "Name=Eclipse C/C++" >> /usr/share/applications/eclipse.desktop'
sudo sh -c 'echo "Comment=Eclipse IDE for C/C++ Developers" >> /usr/share/applications/eclipse.desktop'
sudo sh -c 'echo "Exec=/opt/eclipse/eclipse" >> /usr/share/applications/eclipse.desktop'
sudo sh -c 'echo "Icon=/opt/eclipse/icon.xpm" >> /usr/share/applications/eclipse.desktop'
sudo sh -c 'echo "Terminal=false" >> /usr/share/applications/eclipse.desktop'
sudo sh -c 'echo "Type=Application" >> /usr/share/applications/eclipse.desktop'
sudo sh -c 'echo "Categories=Development;" >> /usr/share/applications/eclipse.desktop'


## Install LaTeX (Ursin)

#sudo apt install texlive texstudio        # +293 MB
sudo apt -y install texlive-latex-extra texstudio  # +475 MB
sudo apt -y install texlive-xetex texlive-fonts-recommended  # +32 MB (needed for jupyterlab pdf export, see above)

# test with: https://services.math.duke.edu/computing/tex/templates.html


## Install Qucs (Ulrich)

# use AppImage from AppImageHub https://appimage.github.io/Qucs-S/ as it needs Qt4 for which 20.04 dropped support
sudo mkdir -p /opt/Qucs-S-0.0.22_x86_64/
sudo wget -O /opt/Qucs-S-0.0.22_x86_64/Qucs-S-0.0.22_x86_64.AppImage https://github.com/ra3xdh/qucs_s/releases/download/0.0.22/Qucs-S-0.0.22_x86_64.AppImage  # +21 MB
sudo chmod +x /opt/Qucs-S-0.0.22_x86_64/Qucs-S-0.0.22_x86_64.AppImage
sudo sh -c 'echo "[Desktop Entry]" >> /usr/share/applications/qucs-s.desktop'
sudo sh -c 'echo "Name=Qucs-S" >> /usr/share/applications/qucs-s.desktop'
sudo sh -c 'echo "Comment=Quite Universal Circuit Simulator" >> /usr/share/applications/qucs-s.desktop'
sudo sh -c 'echo "Exec=/opt/Qucs-S-0.0.22_x86_64/Qucs-S-0.0.22_x86_64.AppImage" >> /usr/share/applications/qucs-s.desktop'
sudo sh -c 'echo "Icon=kturtle" >> /usr/share/applications/qucs-s.desktop'
sudo sh -c 'echo "Terminal=false" >> /usr/share/applications/qucs-s.desktop'
sudo sh -c 'echo "Type=Application" >> /usr/share/applications/qucs-s.desktop'
sudo sh -c 'echo "Categories=Development;Science;" >> /usr/share/applications/qucs-s.desktop'
## compile qucsator manually (cli only - does NOT need Qt4)
#sudo apt -y install gperf
#sudo apt -y install adms
#sudo wget https://master.dl.sourceforge.net/project/qucs/qucs/0.0.20/qucsator-0.0.20-rc2.tar.gz
#sudo tar xf qucsator-0.0.20-rc2.tar.gz -C /tmp
#rm qucsator-0.0.20-rc2.tar.gz
#cd /tmp/qucsator-0.0.20/
#./configure
#make
#make install ...???
#rm -rf /tmp/qucsator-0.0.20


## Install Maxima CAS (Ulrich) - ? FALLS SYMPY ETC. NICHT AUSREICHEND...

#sudo apt -y install wxmaxima              # +140 MB


## Install Arduino/AVR (Ulrich?)         # +581 MB

#wget https://downloads.arduino.cc/arduino-1.8.12-linux64.tar.xz
#sudo tar -xf arduino-1.8.12-linux64.tar.xz -C /opt/
#rm ~/arduino-1.8.12-linux64.tar.xz
#sudo /opt/arduino-1.8.12/install.sh 


## PicoScope 6 (Beta)

#sudo bash -c 'echo "deb https://labs.picotech.com/debian/ picoscope main" >/etc/apt/sources.list.d/picoscope.list'
#wget -O - https://labs.picotech.com/debian/dists/picoscope/Release.gpg.key | sudo apt-key add -
#sudo apt-get update
#sudo apt-get install picoscope            # +408 MB

# confer also; https://www.picotech.com/downloads/linux (works for 20.04)


## Install KiCad EDA

#sudo apt -y install kicad                 # +355 MB


## Install CAD and 3D Printing

#sudo apt -y install freecad               # +479 MB, (allows python scripting)
sudo apt -y install meshlab               # +25 MB


## Post-install config, add desktop links

#cp /usr/share/applications/assistant-qt5.desktop /home/fhgr/Schreibtisch/
#cp /usr/share/applications/designer-qt5.desktop /home/fhgr/Schreibtisch/
cp /usr/share/applications/eclipse.desktop /home/fhgr/Schreibtisch/
#cp /usr/share/applications/info.desktop /home/fhgr/Schreibtisch/
cp /usr/share/applications/jupyter-qtconsole.desktop /home/fhgr/Schreibtisch/
#cp /usr/share/applications/linguist-qt5.desktop /home/fhgr/Schreibtisch/
#cp /usr/share/applications/mc.desktop /home/fhgr/Schreibtisch/
#cp /usr/share/applications/mcedit.desktop /home/fhgr/Schreibtisch/
cp /usr/share/applications/meshlab.desktop /home/fhgr/Schreibtisch/
cp /usr/share/applications/org.gnome.meld.desktop /home/fhgr/Schreibtisch/
#cp /usr/share/applications/org.kde.kate.desktop /home/fhgr/Schreibtisch/
#cp /usr/share/applications/org.kde.okular.desktop /home/fhgr/Schreibtisch/
cp /usr/share/applications/org.qt-project.qtcreator.desktop /home/fhgr/Schreibtisch/
cp /usr/share/applications/pdfarranger.desktop /home/fhgr/Schreibtisch/
#cp /usr/share/applications/picoscope.desktop /home/fhgr/Schreibtisch/
cp /usr/share/applications/qucs-s.desktop /home/fhgr/Schreibtisch/
cp /usr/share/applications/spyder3.desktop /home/fhgr/Schreibtisch/
cp /usr/share/applications/terminator.desktop /home/fhgr/Schreibtisch/
#cp /usr/share/applications/texdoctk.desktop /home/fhgr/Schreibtisch/
cp /usr/share/applications/texstudio.desktop /home/fhgr/Schreibtisch/
chmod +x /home/fhgr/Schreibtisch/*.desktop
ln -s /media/ /home/fhgr/Schreibtisch/media


## Prepare VM disk for deployment

# Clear apt cache
sudo du -sh /var/cache/apt/archives/
sudo apt clean

# Remove this setup script
rm ~/FHGR-Photonics+MobileRobotics-Kubuntu-20.04-setup.sh

# Shrink drive before exporting: https://superuser.com/questions/529149/how-to-compact-virtualboxs-vdi-file-size

echo
echo 'Nullify free space in order to shrink virtual disk afterwards.'
echo 'VM should be run w/o snapshots active, e.g. all should be merged before.'

#read -p "Press any key to continue..."

# https://github.com/chef/bento/blob/master/packer_templates/_common/minimize.sh

# Whiteout root
count=$(df --sync -kP / | tail -n1  | awk -F ' ' '{print $4}')
count=$(($count-1))
sudo dd if=/dev/zero of=/tmp/whitespace bs=1M count=$count || echo "dd exit code $? is suppressed";
sudo rm /tmp/whitespace

# Whiteout /boot
count=$(df --sync -kP /boot | tail -n1 | awk -F ' ' '{print $4}')
count=$(($count-1))
sudo dd if=/dev/zero of=/boot/whitespace bs=1M count=$count || echo "dd exit code $? is suppressed";
sudo rm /boot/whitespace

#read -p "Press any key to continue..."

shutdown -h now
