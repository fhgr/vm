# VM

This is a setup script for the FHGR Photonics + Mobile Robotics VM. Currently it
uses [Kubuntu 22.04(.1) (LTS)](http://cdimage.ubuntu.com/kubuntu/releases/22.04/release/)
and [VirtualBox 6.1.28](https://www.virtualbox.org/wiki/Download_Old_Builds_6_1).

The main purpose of this VM is to provide a pre-installed and setup software
environment for studies and practical work.

Possible ways to automate this setup further include the use of fabric or
[packer](https://github.com/chenhan1218/packer-desktop) on
[travis](https://github.com/travis-ci/packer-templates) as well as using server
or minimal mediums or a pre-setup VM image from osboxes.org.
Instead of using packer a few simple scripts can be used to achive the same
kind of unattended install [[1](https://github.com/hiragashi/ubuntu-unattended-install),
[2](https://github.com/netson/ubuntu-unattended)].

## Featured software

* Python 3 (including numpy, scipy, matplotlib, jupyter, spyder, etc.)
* C/C++ toolchain
* LaTeX
* and more, see [full list](https://gitlab.com/fhgr/vm/-/blob/master/FHGR-Photonics+MobileRobotics-Kubuntu-20.04-setup.sh#L200)
(.sh script beginning from section "Install basic packages")

## Change log

* ([latest](https://gitlab.com/fhgr/vm/-/compare/v2.0...master))
* [v2.0](https://gitlab.com/fhgr/vm/-/compare/v1.2.1...v2.0)
  * updated to Kubuntu 22.04
  * various program updates and fixes
* [v1.2.1](https://gitlab.com/fhgr/vm/-/compare/v1.2...v1.2.1)
  * disable unattended security upgrades: `$ sudo apt -y purge unattended-upgrades`
  * update: [FHGR-Photonics+MobileRobotics-Kubuntu-20.04-update.sh](FHGR-Photonics+MobileRobotics-Kubuntu-20.04-update.sh)
* [v1.2](https://gitlab.com/fhgr/vm/-/compare/v1.1...v1.2)
* [v1.1](https://gitlab.com/fhgr/vm/-/compare/v1.0...v1.1)
* [v1.0](https://gitlab.com/fhgr/vm/-/compare/beta-v0.2...v1.0)
* [beta-v0.2](https://gitlab.com/fhgr/vm/-/compare/beta-v0.1...beta-v0.2)
* [beta-v0.1](https://gitlab.com/fhgr/vm/-/compare/ad59bcd23f9fb08368bfc8629aadb95a90f99b6d...beta-v0.1)

# Build a VM using this repo

1. [Open FHGR-Photonics+MobileRobotics-Kubuntu-22.04-setup.sh](https://gitlab.com/fhgr/vm/-/blob/master/FHGR-Photonics+MobileRobotics-Kubuntu-20.04-setup.sh) (make sure to select the proper tag) 
2. Follow the instructions (the file contains the instructions and is a script to build the VM)
3. All other files or data required will be downloaded by the script automatically

# Update a VM using this repo
1. ~~[Open FHGR-Photonics+MobileRobotics-Kubuntu-20.04-update.sh](https://gitlab.com/fhgr/vm/-/blob/master/FHGR-Photonics+MobileRobotics-Kubuntu-20.04-update.sh) (make sure to select the proper tag)~~ 
2. ~~Follow the instructions (the file contains the instructions and is a script to build the VM)~~
3. ~~All other files or data required will be downloaded by the script automatically~~
# Review / Feedback

[pho: organisation FS21 / Umfrage zur Umstellung auf Python und der VM / Umfrage zur Umstellung auf Python und der VM (für Dozenten)](https://moodle.fhgr.ch/mod/feedback/analysis.php?id=353753)

Issues:
- [ ] camera hardware config close to impossible (integration time, etc.)
- [ ] USB hardware sometimes not working
- [x] boot up time; create snapshot in running state
- [x] shared drive to clumsy due to moving of data; use exactly same directory as under win for data (no moving needed)

# TODO

## Next update (v2.0.1)

Config:
- [ ] "Update a VM using this repo" für v2.0 erstellen
- [ ] Doku anpassen für v2.0 (Kubuntu 22.04). Evt. Doku ins Repo nehmen
- [ ] Readme anpassen für v2.0
- [ ] Anzeige > Bildschirm > Grafikspeicher: 64 MB
  - dachte ich hätte das als Standard gesetzt aber einige Studenten hatten nur 16 MB
- [ ] Kubuntu Bildschirmschoner bzw. -sperre deaktivieren
  - das war in v1.2 gemacht aber bei 1 Student wurde die Sperre aktiv (ev. wurde die Einstellung durch Update verändert?)
- [ ] Die «Konsole» startet in der Grösse von 34 x 4 – das sollten wir ändern v.a. weil es für mich das wichtigste/meistgestartete Programm ist
- [ ] In Spyder könnten wir “Check for update” ausschalten – kommt jetzt schon regelmässig und stört eher
- [ ] War der Ordner «/media/fhgr» in den früheren Versionen der VM auch vorhanden? Hast Du eine Idee warum?

Add:
- [ ] pywt/pywavelets; installed but should be made explicit for "Ursin"
- [ ] f3d; remove meshlab and use f3d instead (f3d is available for win, so no install in vm needed - thus just remove meshlab)
- [ ] Visual Studio Code; für "Philipp" - besteht Bedarf?
- [ ] ( exiftool; install [exiftool (apt)](https://wiki.ubuntuusers.de/ExifTool/) including [PyExifTool (pip3)](https://pypi.org/project/PyExifTool/) and/or exiv2 (apt) including py3exiv2 (pip3) )
- [ ] ( pymssa; may be install [pymssa](https://github.com/kieferk/pymssa) SSA )

Docu:
- [ ] Negative Punkte auch auflisten; Ressourcenverbrauch, ev. Probleme mit Installation und physischer HW, grundlegendes Verständnis für Virtualisierung nötig (bzw. sehr hilfreich)
